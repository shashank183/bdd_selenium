package Config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.ie.ElementScrollBehavior;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import static Util.PropertiesUtil.properties;
import static Config.ResourcePath.Chromedriverpath;

public class DriverConfig {
    public long DEFAULT_WAIT = 10;

    public WebDriver driver;

    public WebDriver getDriver() {
        if (driver == null || driver.toString().contains("null")) {
            driver = GetWebDriver(properties.getProperty("Browser"));

        }
        return driver;
    }

    public WebDriver GetWebDriver(String browser) {
        if (browser.equalsIgnoreCase("CHROME")) {
            String chromeDriverLocation = Chromedriverpath;
            System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, chromeDriverLocation);
            DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
            chromeCapabilities.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR, ElementScrollBehavior.BOTTOM);
            chromeCapabilities.setCapability("takeScreenShot", true);
            driver = new ChromeDriver();

        }
        return driver;
    }

}

