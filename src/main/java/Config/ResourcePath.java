package Config;

public class ResourcePath {

    public static String userdir = System.getProperty("user.dir");
    public static String CommonConfigPath = userdir+"\\src\\main\\resources\\Properties\\CommonConfig.properties";
    public static String TestEnvConfigPath = userdir+"\\src\\main\\resources\\Properties\\TestEnv.properties";
    public static String Chromedriverpath = userdir+"\\src\\main\\resources\\Drivers\\chromedriver.exe";


}
