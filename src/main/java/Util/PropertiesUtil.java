package Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import static Config.ResourcePath.CommonConfigPath;
import static Config.ResourcePath.TestEnvConfigPath;


public class PropertiesUtil {
    static Logger logger = LoggerFactory.getLogger(PropertiesUtil.class);

    public static final Properties properties = GetProperties();

    private static Properties GetProperties(){

        Properties property = new Properties();

                       try{
                   FileInputStream configfile = new FileInputStream(new File(CommonConfigPath));
                           property.load(configfile);
               }
               catch (Exception exception){
                   System.out.println("Exception Occured,plz find relevant message: "+exception.getMessage());
                   logger.info("Exception Occured,plz find relevant message: "+exception.getMessage());
               }
        property.putAll(GetEnvProperties());
                       return property;

    }

   @org.jetbrains.annotations.NotNull
   private  static Properties GetEnvProperties(){
        Properties envconfigproperty = new Properties();
        try{
                        FileInputStream envpropfile = new FileInputStream((new File(TestEnvConfigPath)));
                        envconfigproperty.load(envpropfile);

        }catch (Exception exception){
            System.out.println("Exception Occured,plz find relevant message: "+exception.getMessage());
            logger.info("Exception Occured,plz find relevant message: "+exception.getMessage());
        }
        return envconfigproperty;
   }
}
